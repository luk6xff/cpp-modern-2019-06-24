#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include "container.hpp"

#include "catch.hpp"

using namespace std;

string full_name(const string& fn, const string& ln)
{
    return fn + " " + ln;
}

TEST_CASE("reference binding")
{
    SECTION("cpp98")
    {
        string fn = "jan";
        string& ref_fn = fn;

        const string& ref_name = full_name("jan", "kowalski");
    }

    SECTION("since cpp11")
    {
        string fn = "jan";
        string&& rvref_name = full_name("jan", "kowalski");
        rvref_name[0] = 'p';

        cout << rvref_name << "\n";

        //string&& ill_formed = fn; // ERROR
    }
}

vector<int> load_data(const string& file_name)
{
    vector<int> data;
    data.reserve(1'000'000);

    for(int i = 0; i < 1'000'000; ++i)
        data.push_back(i);

    return data;
}

std::array<int, 100> create_array()
{
    std::array<int, 100> data = { 1, 2, 3, 4 };

    return data;
}

TEST_CASE("move semantics")
{
    SECTION("moving objects")
    {
        vector<int> vec1 = { 1, 2, 3, 4 };

        vector<int> vec2 = vec1; // copy

        vector<int> vec3 = std::move(vec2);

        vec2 = vec1; // assignment of a new state

        vector<int> vec = load_data("big_data.txt");

        std::array<int, 100> data = create_array();
    }
}

ModernCpp::Container load_data_nrvo()
{
    ModernCpp::Container cont = {11, 222, 3333};

    return cont;
}

ModernCpp::Container load_data()
{
    return ModernCpp::Container{11, 222, 3333};
}

TEST_CASE("Container")
{
    using namespace ModernCpp;

    Container c1 = {1, 2, 3, 4};

    const Container c2 = c1;

    Container c3 = std::move(c2);

    Container c4 = load_data(); // n-rvo

    cout << "\n---------- push backs:\n";
    vector<Container> dataset;
    //dataset.reserve(100);

    for(int i = 0; i < 10; ++i)
    {
        cout << "\n----\n";
        dataset.push_back(load_data());
    }

//    dataset.push_back(load_data());
//    dataset.push_back(load_data());
//    dataset.push_back(load_data());
//    dataset.push_back(std::move(c1));
//    dataset.push_back(std::move(c4));
}

//TEST_CASE("DataSet")
//{
//    using namespace ModernCpp;

//    cout << "\n";

//    DataSet ds;

//    DataSet ds2 = std::move(ds);

//    vector<DataSet> big_data;

//    big_data.push_back(std::move(ds2));
//}


//template <typename T,typename Arg1>
//std::unique_ptr<T> my_make_unique(Arg1&& arg1)
//{
//    return std::unique_ptr<T>(new T(std::forward<Arg1>(arg1)));
//}

//template <typename T,typename Arg1, typename Arg2>
//std::unique_ptr<T> my_make_unique(Arg1&& arg1, Arg2&& arg2)
//{
//    return std::unique_ptr<T>(new T(std::forward<Arg1>(arg1), std::forward<Arg2>(arg2)));
//}

template <typename T,typename... Args>
std::unique_ptr<T> my_make_unique(Args&&... args)
{
    return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}

TEST_CASE("perfect forwarding")
{
    using namespace ModernCpp;

    {
        unique_ptr<Container> uptr{new Container(10, 665)};
        auto uptr2 = my_make_unique<Container>(10, 665);
        auto uptr3 = my_make_unique<vector<string>>(10, "abc"s);
        auto uptr4 = my_make_unique<tuple<int, double, string>>(1, 3.14, "text");
        auto uptr5 = my_make_unique<string>();
    }

    vector<Container> dataset;

    dataset.push_back(Container{1, 2, 3});
    dataset.push_back(Container(10, 667));
    dataset.emplace_back(20, -1);
    //dataset.emplace_back({5, 6, 7});
}
