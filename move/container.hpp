#ifndef CONTAINER_HPP
#define CONTAINER_HPP

#include <algorithm>
#include <cstddef>
#include <vector>

namespace ModernCpp
{
    class Container
    {
        int* array_;
        size_t size_;

    public:
        using iterator = int*;
        using const_iterator = const int*;

        Container(size_t size, int value)
            : array_{new int[size]}
            , size_{size}
        {
            std::fill(begin(), end(), value);
        }

        Container(std::initializer_list<int> il)
            : array_{new int[il.size()]}
            , size_{il.size()}
        {
            std::copy(il.begin(), il.end(), array_);

            std::cout << "Container({";
            for (size_t i = 0; i < size_; ++i)
                std::cout << array_[i] << " ";
            std::cout << "})\n";
        }

        Container(const Container& other)
            : array_{new int[other.size_]}
            , size_{other.size_}
        {
            std::copy(other.begin(), other.end(), array_);

            std::cout << "Container(cc: {";
            for (size_t i = 0; i < size_; ++i)
                std::cout << array_[i] << " ";
            std::cout << "})\n";
        }

        Container& operator=(const Container& other)
        {
            if (this != &other)
            {
                auto temp = new int[other.size_];

                delete[] array_;
                array_ = temp;

                size_ = other.size_;
                std::copy(other.begin(), other.end(), array_);

                std::cout << "Container(cp=: {";
                for (size_t i = 0; i < size_; ++i)
                    std::cout << array_[i] << " ";
                std::cout << "})\n";
            }

            return *this;
        }

        Container(Container&& other) noexcept
            : array_{other.array_}
            , size_{other.size_}
        {
            other.array_ = nullptr;
            other.size_ = 0;

            std::cout << "Container(mv: {";
            for (size_t i = 0; i < size_; ++i)
                std::cout << array_[i] << " ";
            std::cout << "})\n";
        }

        Container& operator=(Container&& other) noexcept
        {
            if (this != &other)
            {
                delete[] array_;
                array_ = other.array_;
                size_ = other.size_;
                other.array_ = nullptr;                                
            }

            std::cout << "Container(mv=: {";
            for (size_t i = 0; i < size_; ++i)
                std::cout << array_[i] << " ";
            std::cout << "})\n";

            return *this;
        }

        ~Container() noexcept
        {
            std::cout << "~Container(";

            if (array_ == nullptr)
            {
                std::cout << "after move";
            }
            else
            {
                std::cout << "{";
                for (size_t i = 0; i < size_; ++i)
                    std::cout << array_[i] << " ";
                std::cout << "}";
            }

            std::cout << ")\n";

            delete[] array_;
        }

        int& operator[](size_t index) noexcept
        {
            return array_[index];
        }

        const int& operator[](size_t index) const noexcept
        {
            return array_[index];
        }

        iterator begin() noexcept
        {
            return array_;
        }

        iterator end() noexcept
        {
            return array_ + size_;
        }

        const_iterator begin() const noexcept
        {
            return array_;
        }

        const_iterator end() const noexcept
        {
            return array_ + size_;
        }
    };

    struct DataSet
    {
        std::vector<int> numbers;
        Container data;

        DataSet(DataSet&&) = default;
        
        DataSet()
            : numbers{1, 2, 3}, data{665, 666, 667}
        {

        }
    };

} // namespace ModernCpp

#endif
