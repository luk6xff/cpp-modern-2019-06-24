#include <algorithm>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

#include "catch.hpp"
#include "container.hpp"

using namespace std;

struct Point
{
    int x, y;
};

struct Vector2D
{
    int x, y;

    Vector2D(int x, int y)
        : x{x}
        , y{y}
    {
    }
};

long gen_id()
{
    static long seed = 0;

    return ++seed;
}

TEST_CASE("uniform init - {}")
{
    int x1{}; // value set to zero
    int x2{42};
    //int x3{gen_id()};

    int* ptr{};

    Point pt1{};
    REQUIRE((pt1.x == 0 && pt1.y == 0));

    Point pt2{1};
    REQUIRE((pt2.x == 1 && pt2.y == 0));

    Vector2D vec1{1, 4};

    const vector<Vector2D> vecs = {{1, 4}, {6, 7}, {7, 8}};

    int* ptr2 = new int{10};
}

TEST_CASE("initializer list")
{
    using namespace ModernCpp;

    Container cont = {1, 2, 3, 4}; // calls Container(initializer_list<int>)
    Container backup{cont};

    for (const auto& item : cont)
        cout << item << " ";
    cout << "\n";

    REQUIRE(cont[0] == 1);

    Container cont2{10, -1}; // // calls Container(initializer_list<int>)

    for (const auto& item : cont2)
        cout << item << " ";
    cout << "\n";

    Container cont3(10, -1); // // calls Container(size_t, int)

    for (const auto& item : cont3)
        cout << item << " ";
    cout << "\n";

    cout << "\n=========\n";

    vector<int> vec1{10, -1}; // [10, -1]
    for (const auto& item : vec1)
        cout << item << " ";
    cout << "\n";

    vector<int> vec2(10, 1); // [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
    for (const auto& item : vec2)
        cout << item << " ";
    cout << "\n";
}
