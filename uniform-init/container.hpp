#ifndef CONTAINER_HPP
#define CONTAINER_HPP

#include <algorithm>
#include <cstddef>

namespace ModernCpp
{
    class Container
    {
        int* array_;
        size_t size_;

    public:
        using iterator = int*;
        using const_iterator = const int*;

        Container(size_t size, int value)
            : array_{new int[size]}
            , size_{size}
        {
            std::fill(begin(), end(), value);
        }

        Container(std::initializer_list<int> il)
            : array_{new int[il.size()]}
            , size_{il.size()}
        {
            std::copy(il.begin(), il.end(), array_);
        }

        int& operator[](size_t index)
        {
            return array_[index];
        }

        iterator begin()
        {
            return array_;
        }

        iterator end()
        {
            return array_ + size_;
        }

        const_iterator begin() const
        {
            return array_;
        }

        const_iterator end() const
        {
            return array_ + size_;
        }
    };

} // namespace ModernCpp

#endif
