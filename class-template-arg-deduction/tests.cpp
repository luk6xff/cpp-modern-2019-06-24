#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

template <typename T1, typename T2>
struct ValuePair
{
    T1 fst;
    T2 snd;

    ValuePair(const T1& arg1, const T2& arg2) : fst{arg1}, snd{arg2}
    {}
};


// deduction guide
template <typename T1, typename T2>
ValuePair(T1, T2) -> ValuePair<T1, T2>;

template <typename T>
ValuePair(T, const char*) -> ValuePair<T, string>;


template <typename T1, typename T2>
ValuePair<T1, T2> make_vpair(T1 a, T2 b)
{
    return ValuePair<T1, T2>(a, b);
}

template <typename T>
struct Container
{
    vector<T> vec;

    Container(initializer_list<T> lst) : vec{lst}
    {}
};

TEST_CASE("CTAD")
{
    ValuePair<int, double> v1{1, 3.14};
    auto v2 = make_vpair(1, 3.14);

    SECTION("since C++17")
    {
        ValuePair v3{3, "text"s}; // ValuePair<int, string>
        ValuePair v4(3.14, "text"); // ValuePair<double, const char*>

        const int x = 10;
        const int& y = 20;

        ValuePair v5{x, y};
        static_assert(is_same_v<decltype(v5), ValuePair<int, int>>);

        Container c1 = { 1, 2, 3, 4 };
        Container c2{ 1, 2, 3, 4 };
        Container c3({1, 2, 3});

        vector vec = {1, 2, 3, 4}; // vector<int>
        vector vec2(vec); // vector<int>
    }

    SECTION("function")
    {
        function f = [](int a, const int& b) { return a + b; };
        f = [](int a, const int& b) { cout << a << ", " << b << "\n"; return 32; };
    }
}


template <typename... T>
auto make_vec(T... args)
{
    vector vec{args...};
    return vec;
}
