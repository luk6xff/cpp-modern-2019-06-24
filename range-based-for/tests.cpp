#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

TEST_CASE("range-based for")
{
    SECTION("works with containers")
    {
        vector<string> vec = {"1", "2", "3", "4"};

        for(const auto& item : vec)
        {
            cout << item << " ";
        }

        //for_each(begin(vec), end(vec), [](const auto& item) { cout << item << " ";});
        //cout << "\n";

        cout << "\n";

        SECTION("is interpreted as")
        {
            for(auto it = vec.begin(); it != vec.end(); ++it)
            {
                const auto& item = *it;
                cout << item << " ";
            }
            cout << "\n";
        }

        vector<bool> flags = {1, 0, 0, 0, 1};

        for (auto&& flag : flags)
        {
            flag.flip();
        }

        for (const auto& flag : flags)
        {
            cout << flag << " ";
        }
        cout << "\n";
    }

    SECTION("works with native arrays")
    {
        int tab[3] = {1, 2, 3};

        for(const auto& item : tab)
        {
            cout << item << "\n";
        }
        cout << "\n";

        SECTION("is interpreted as")
        {
            for(auto it = begin(tab); it != end(tab); ++it)
            {
                const auto& item = *it;
                cout << item << " ";
            }
            cout << "\n";
        }
    }

    SECTION("works with initializer lists")
    {
        for(const auto& item : {"one", "two", "three"})
        {
            cout << item << " ";
        }
        cout << "\n";
    }

    SECTION("range based for with move")
    {
        vector<string> target = { "zero" };

        vector<string> data = { "one", "two", "three" };

        for(auto&& item : vector{ "one"s, "two"s, "three"s })
        {
            target.push_back(std::move(item));
        }
    }
}
