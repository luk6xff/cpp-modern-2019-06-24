#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

const double pi = 3.1415;

template <typename T>
struct Pi
{
    static constexpr T value = 3.1415;
};

template <typename T>
constexpr T e = 2.7;

TEST_CASE("variable templates")
{
    cout << Pi<double>::value << "\n";
    cout << Pi<int>::value << "\n";

    cout << e<double> << "\n";
    cout << e<int> << "\n";

    int x = 10;
    static_assert(is_same<int, decltype(x)>::value);
    static_assert(is_same_v<int, decltype(x)>);
}

void variadic_print()
{
    cout << "\n";
}

template <typename Head, typename... Tail>
void variadic_print(const Head& head, const Tail&... tail)
{
    cout << head << " ";
    variadic_print(tail...);
}

namespace Cpp17
{
    template <typename Head, typename... Tail>
    void variadic_print(const Head& head, const Tail&... tail)
    {
        cout << head << " ";
        if constexpr(sizeof...(tail))
        {
            variadic_print(tail...);
        }
        else
        {
            cout << "\n";
        }
    }
}

TEST_CASE("variadic templates")
{
    variadic_print(1, 2.7, "text");
    variadic_print(1, 2.7, "text", "abc"s, 3.14);
}

template <typename Head>
auto sum(Head head)
{
    return head;
}

template <typename Head, typename... Tail>
auto sum(Head head, Tail... tail)
{
    return head + sum(tail...);
}

namespace Cpp17
{
    template <typename Head, typename... Tail>
    constexpr auto sum_constexpr(Head head, Tail... tail)
    {
        if constexpr(sizeof...(tail) == 0)
        {
           return head;
        }
        else
        {
           return head + sum(tail...);
        }
    }


    template <typename... Args>
    common_type_t<Args...> sum(Args... args)
    {
        using ResultType = common_type_t<Args...>;

        return (... + static_cast<ResultType>(args)); // left fold with +
    }

    template <typename... Args>
    void print(const Args&... args)
    {
        auto with_space = [](auto arg) { return arg + " "; };

        (cout << ... << with_space(args)) << "\n";
    }
}

template <typename... Ts>
auto from_list(Ts&&... args) // -> vector<common_type_t<Ts...>>
{
    vector<common_type_t<Ts...>> vec;
    vec.resize(sizeof...(args));

    (vec.push_back(std::forward<Ts>(args)), ...);

    return vec;
}

TEST_CASE("sum")
{
    REQUIRE(sum(1, 3, 5, 3) == 12);
    REQUIRE(Cpp17::sum("abc", "def"s, "ghi", "jkl") == "abcdefghijkl"s);

    const vector<unique_ptr<int>> vec = from_list(make_unique<int>(1), make_unique<int>(2));
}


