#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

TEST_CASE("raw-string literal")
{
    string path1 = "C:\\nasz katalog\\backup";

    const char* path2 = R"(C:\nasz katalog\backup)";

    REQUIRE(path1 == path2);

    SECTION("multiline")
    {
        string text = R"(Line1
Line2
Line3)";

        cout << text << endl;
    }

    SECTION("custom delimiter")
    {
        string str = R"raw(cytat "(tekst)" po cytacie)raw";

        cout << str << "\n";
    }
}

void foo(const char* text)
{
    cout << "foo(c-string: " << text << ")\n";
}

void foo(const std::string& text)
{
    cout << "foo(std::string: " << text << ")\n";
}


TEST_CASE("string literal - since C++14")
{
    auto text1 = "test"; // c-string literal
    auto text2 = "test"s; // std::string

    foo("text");
    foo("text"s);

    REQUIRE("abc"s == "abc"s); // using operator== for std::string
}
