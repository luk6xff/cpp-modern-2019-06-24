# Programming in modern C++

## Docs ##

* https://infotraining.bitbucket.io/cpp-modern/
* https://infotraining.bitbucket.io/cpp-thd/

## Links ##

* http://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines
* https://herbsutter.com/2017/07/26/metaclasses-thoughts-on-generative-c

## Ankieta ##

* https://www.infotraining.pl/ankieta/cpp-17-2019-06-26-kp
