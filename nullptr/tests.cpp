﻿#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

void foo(int* ptr)
{
    if (ptr != nullptr)
        cout << "foo(int*: " << *ptr << ")\n";
    else
        cout << "foo(int*: null pointer)\n";
}

void foo(long value)
{
    cout << "foo(long: " << value << ")\n";
}

void foo(nullptr_t)
{
    cout << "foo(nullptr_t)\n";
}

int* alloc_int()
{
    int* ptr = new (nothrow) int(13);
    return ptr;
}

TEST_CASE("nullptr")
{
    int x = 10;
    foo(&x);
    foo(NULL); // calls foo(long)

    foo(nullptr);

    SECTION("init pointers")
    {
        int* ptr = nullptr;
        foo(ptr);

        int* ptr2{};
        REQUIRE(ptr2 == nullptr);
    }

    SECTION("alloc_int")
    {
        int* ptr_int = alloc_int();

        if (ptr_int != nullptr)
        {
            cout << "allocated: " << *ptr_int << endl;
        }
    }
}
