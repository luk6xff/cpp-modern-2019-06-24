#include <algorithm>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

class ClosureClass_46325478135
{
public:
    auto operator()() const { cout << "hello from lambda!\n"; }
};

class ClosureGenericLambda_274278345
{
public:
    template <typename A1, typename A2>
    auto operator()(const A1& a, const A2& b) const
    {
        return a + b;
    }
};

TEST_CASE("lambda expressions")
{
    auto l1 = [] { cout << "hello from lambda!\n"; };
    l1();

    SECTION("is interpreted as")
    {
        auto l1 = ClosureClass_46325478135{};
        l1();
    }

    auto multiply = [](int a, int b) { return a * b; };

    REQUIRE(multiply(3, 5) == 15);

    SECTION("since C++14")
    {
        auto gl_add = [](const auto& a, const auto& b) { return a + b; };

        auto result1 = gl_add(1, 2);
        REQUIRE(result1 == 3);

        REQUIRE(gl_add("abc"s, "cde"s) == "abccde");
    }

    vector<shared_ptr<int>> vec = {make_shared<int>(15), make_shared<int>(2), make_shared<int>(10), make_shared<int>(1)};
    sort(begin(vec), end(vec), [](const auto& a, const auto& b) { return *a < *b; });

    for (const auto& p : vec)
        cout << *p << " ";
    cout << "\n";
}

TEST_CASE("captures & closures")
{
    vector<int> vec = {1, 2, 3, 4};

    SECTION("capture by ref")
    {
        int sum{};

        for_each(begin(vec), end(vec), [&sum](int x) { sum += x; });
    }

    SECTION("capture by value")
    {
        int factor = 5;
        auto by_factor = [factor](int x) { return x * factor; };

        factor = 10;

        transform(begin(vec), end(vec), begin(vec), by_factor);

        for (const auto& item : vec)
            cout << item << " ";
        cout << endl;
    }

    SECTION("capture by move - since C++14")
    {
        unique_ptr<string> up = make_unique<string>("text");

        {
            auto printer = [unique_text = std::move(up)](const string& str) { cout << *unique_text << " - " << str; };

            REQUIRE(up.get() == nullptr);
            printer(" from lambda\n");
        } // text is deallocated
    }

    SECTION("capture list")
    {
        int factor = 5;
        int sum = 0;

        for_each(begin(vec), end(vec), [factor, &sum](int x) { sum += x * factor; });
    }
}

auto create_generator(int seed, int step = 1)
{
    return [seed, step]() mutable { seed += step; return seed; };
}

TEST_CASE("lambdas are immutable")
{
    int x = 10;

    auto l = [x]() mutable { return ++x; };

    REQUIRE(l() == 11);
    REQUIRE(l() == 12);

    vector<int> vec(10);

    generate(begin(vec), end(vec), create_generator(100, 5));

    for (const auto& item : vec)
        cout << item << " ";
    cout << endl;
}

TEST_CASE("storing closures")
{
    SECTION("auto")
    {
        auto do_it = [] { return "XXX"s; };

        REQUIRE(do_it() == "XXX"s);
    }

    SECTION("lambda expr with empty closure list - function pointer")
    {
        auto action = []() { cout << "action\n"; };

        void (*callback)() = action;
        callback();

        callback = []() { cout << "another action\n"; };
        callback();
    }

    SECTION("std::function")
    {
        string prefix = "Text: ";

        std::function<void()> f = [prefix]() { cout << prefix << "YYY\n"; };
        f();
    }
}

template <typename F>
void foo(F fun)
{
    fun();
}

TEST_CASE("passing function as argument")
{
    foo([] { cout << "lambda passed as param\n"; });
}

template <typename Fun>
class CallWrapper
{
    Fun fun;

public:
    template <typename Fun2>
    CallWrapper(Fun2&& f)
        : fun{std::forward<Fun2>(f)}
    {
    }

    void call()
    {
        fun();
    }
};

#if __cplusplus >= 201703L

// deduction guide - since C++17
template <typename Fun2>
CallWrapper(Fun2)->CallWrapper<Fun2>;

#endif

template <typename Fun>
CallWrapper<Fun> make_call_wrapper(Fun&& f)
{
    return CallWrapper<std::decay_t<Fun>>(std::forward<Fun>(f));
}

TEST_CASE("lambda as member - since C++17")
{
    CallWrapper cw{[]() { cout << "hello from lambda as a member...\n"; }};

    cw.call();

    auto action = [] { cout << "AAA\n"; };
    CallWrapper cw2{action};
}