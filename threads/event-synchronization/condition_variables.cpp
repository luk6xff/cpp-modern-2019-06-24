#include <algorithm>
#include <atomic>
#include <condition_variable>
#include <iostream>
#include <mutex>
#include <thread>
#include <vector>
#include <numeric>

using namespace std;

namespace atomic_impl
{
    class Data
    {
        vector<int> data_;
        atomic<bool> is_ready_{false};

    public:
        void read()
        {
            cout << "Start reading..." << endl;

            this_thread::sleep_for(chrono::milliseconds(2000));

            data_.resize(100);
            generate(data_.begin(), data_.end(), [] { return rand() % 100; });

            is_ready_.store(true, memory_order_release);

            cout << "End reading..." << endl;
        }

        void process(int id)
        {
            while (!is_ready_.load(memory_order_acquire))
                ;

            long sum = accumulate(data_.begin(), data_.end(), 0L);

            cout << "Id: " << id << " Sum: " << sum << endl;
        }
    };

    void use_data()
    {
        Data data;

        thread thd1{[&data] { data.read(); }};

        thread thd2{[&data] { data.process(1); }};
        thread thd3{[&data] { data.process(2); }};

        thd1.join();
        thd2.join();
        thd3.join();
    }
}

namespace condition_vars_impl
{
    class Data
    {
        vector<int> data_;
        bool is_ready_ = false;
        mutex cv_mtx_;
        condition_variable cv_;

    public:
        void read()
        {
            cout << "Start reading..." << endl;

            this_thread::sleep_for(chrono::milliseconds(2000));

            data_.resize(100);
            generate(data_.begin(), data_.end(), [] { return rand() % 100; });

            // powiadomienie o zajsciu warunku
            unique_lock<mutex> lk(cv_mtx_);
            is_ready_ = true;
            lk.unlock();
            cv_.notify_all();

            cout << "End reading..." << endl;
        }

        void process(int id)
        {
            unique_lock<mutex> lk(cv_mtx_);

            cv_.wait(lk, [this] { return is_ready_; });

            lk.unlock();

            long sum = accumulate(data_.begin(), data_.end(), 0L);

            cout << "Id: " << id << " Sum: " << sum << endl;
        }
    };

    void use_data()
    {
        Data data;

        thread thd1{[&data] { data.read(); }};

        thread thd2{[&data] { data.process(1); }};
        thread thd3{[&data] { data.process(2); }};
        thread thd4{[&data] { data.process(3); }};
        thread thd5{[&data] { data.process(4); }};

        thd1.join();
        thd2.join();
        thd3.join();
        thd4.join();
        thd5.join();
    }
}

int main()
{
    //atomic_impl::use_data();

    condition_vars_impl::use_data();
}
